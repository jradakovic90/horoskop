﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using WebApplication5.Models;



namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {
       

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Opis horoskopskih znaka.";

            return View();
        }

        [HttpPost]
        public ActionResult Datum(Datum model)
        {


            if ((model.mesec == 3 && model.dan >= 21) || (model.mesec == 4 && model.dan <= 19))
            {
                
                model.znak= "Ovan";
                model.opis = "Ovan je uporan";
            }
            else if ((model.mesec == 4 && model.dan >= 20) || (model.mesec == 4 && model.dan <= 20))
            {
                model.znak="Bik";
                model.opis = "Bik je uporan";
            }
            else if ((model.mesec == 5 && model.dan >= 21) || (model.mesec == 6 && model.dan <= 21))
            {
                model.znak = "Blizanci";
                model.opis = "Blizanac je uporan";
            }
            else if ((model.mesec == 6 && model.dan >= 22) || (model.mesec == 7 && model.dan <= 22))
            {
                model.znak  = "Rak";
                model.opis = "Rak je uporan";
            }
            else if ((model.mesec == 7 && model.dan >= 23) || (model.mesec == 8 && model.dan <= 22))
            {
                model.znak  = "Lav";
                model.opis = "Lav je uporan";
            }
            else if ((model.mesec == 8 && model.dan >= 23) || (model.mesec == 9 && model.dan <= 22))
            {
                model.znak  = "Devica";
                model.opis = "Devica je uporan";
            }
            else if ((model.mesec == 9 && model.dan >= 23) || (model.mesec == 10 && model.dan <= 22))
            {
                model.znak  = "Vaga";
                model.opis = "Vaga je uporan";
            }
            else if ((model.mesec == 10 && model.dan >= 23) || (model.mesec == 11 && model.dan <= 21))
            {
                model.znak  = "Skorpija";
                model.opis = "Skorpija je uporan";
            }
            else if ((model.mesec == 11 && model.dan >= 22) || (model.mesec == 12 && model.dan <= 21))
            {
                model.znak  = "Strelac";
                model.opis = "Strelac je uporan";
            }
            else if ((model.mesec == 12 && model.dan >= 22) || (model.mesec == 1 && model.dan <= 19))
            {
                model.znak  = "Jarac";
                model.opis = "Jarac je uporan";
            }
            else if ((model.mesec == 1 && model.dan >= 20) || (model.mesec == 2 && model.dan <= 18))
            {
                model.znak  = "Vodolija";
                model.opis = "Vodolija je uporan";
            }
            else if ((model.mesec == 2 && model.dan >= 19) || (model.mesec == 3 && model.dan <= 20))
            {
                model.znak  = "Ribe";
                model.opis = "Ribe je uporan";
            }




            return View("Znak", model);
        }
        public ActionResult Znak()
        {
            ViewBag.Message = "Horoskopski znak";

            return View();
        }


    }
}