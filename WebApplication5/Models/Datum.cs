﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{

    public class Datum
    {
        [Required]
        public int dan { get; set; }
        [Required]
        public int mesec { get; set; }
      

        public String znak { get; set; }

        public String opis { get; set; }

    }
}